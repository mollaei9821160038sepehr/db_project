create table if not exists "Doctor"(
	dr_id smallserial,
	full_name varchar(50) not null,
	special varchar(20) not null,
	age Integer,
	phone_number varchar(20) not null unique,
	primary key (dr_id)
);

create table if not exists "Week"(
	day_str varchar(20) not null,
	week_day Integer not null,
	primary key(week_day)
);

create table if not exists "Work_time"(
	start_time TIME not null,
	end_time TIME not null,
	week_day Integer not null,
	room_num Integer not null,
	dr_id Integer not null,
	primary key(start_time, end_time, week_day, room_num),
	foreign key (dr_id) REFERENCES "Doctor",
	foreign key(week_day) REFERENCES "Week"
);

create table if not exists "Patient"(
	id_p smallserial,
	full_name varchar(50) not null,
	birthDay DATE,
	address varchar(150),
	blood_type varchar(5),
	primary key (id_p)
);

create table if not exists "Section"(
	section_id smallserial,
	id_p Integer not null,
	dr_id Integer not null,
	time_slot TIMESTAMP not null,
	cost money not null, 
	room_num Integer not null,
	primary key(section_id),
	foreign key(id_p) REFERENCES "Patient"

);

create table if not exists "Drug"(
	drug_barcode smallserial,
	produce_date DATE not null,
	exp_date DATE not null,
	name varchar(30) not null,
	cost money not null,
	primary key(drug_barcode)
);

create table if not exists "Prescribe"(
	id_p Integer not null,
	dr_id Integer not null,
	info varchar(100) not null, 
	prescribe_date DATE not null,
	id_prescribe smallserial,
	primary key(id_prescribe),
	foreign key(id_p) REFERENCES "Patient",
	foreign key(dr_id) REFERENCES "Doctor"
);

create table if not exists "Basket_drug"(
	id_prescribe Integer not null,
	drug_barcode Integer not null,
	primary key(id_prescribe, drug_barcode),
	foreign key(id_prescribe) REFERENCES "Prescribe",
	foreign key(drug_barcode) REFERENCES "Drug"
);

/** Insert **/

-- Resetting data
delete from "Basket_drug";
delete from "Work_time";
delete from "Section";
delete from "Prescribe";
delete from "Doctor";
delete from "Week";
delete from "Patient";
delete from "Drug";



-- Week
insert into "Week" values ('SAT', 1);
insert into "Week" values ('SUN', 2);
insert into "Week" values ('MON', 3);
insert into "Week" values ('TUE', 4);
insert into "Week" values ('WED', 5);
insert into "Week" values ('TEU', 6);
insert into "Week" values ('FRI', 7);


-- Doctor
insert into "Doctor" (dr_id, full_name, special, age, phone_number) values(1 ,'JOhnson', 'General', 56, '+982188991202');
insert into "Doctor" (dr_id, full_name, special, age, phone_number) values(2 ,'PETER', 'Heart', 23, '+98132305697');


-- Inserting data for doctor work time
insert into "Work_time" (dr_id, start_time, end_time, week_day, room_num) values(1, '7:00', '7:30', 2, 3); 
insert into "Work_time" (dr_id, start_time, end_time, week_day, room_num) values(1, '03:00', '04:30', 1, 2);


-- Patient
insert into "Patient" (id_p, full_name, birthday, address, blood_type) values(1, 'Sam', '1988-12-9', '1', 'A+');


-- Section
insert into "Section" (section_id, id_p, dr_id, time_slot, cost, room_num) values(1, 1, 1, '2022-07-15 14:30', '2563',  3);


-- Prescribe
insert into "Prescribe"(id_prescribe, id_p, dr_id, info, prescribe_date) values (2, 1, 1, 'ٍاضطراری', '2024-09-14');


-- Drug
insert into "Drug"(drug_barcode, produce_date, exp_date, name, cost) values (455, '2023-01-11', '2023-01-11', 'آنتی هیستامین', '56545');


-- Basket_drug
insert into "Basket_drug"(id_prescribe, drug_barcode) values (2 , 455);


